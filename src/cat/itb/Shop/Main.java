package cat.itb.Shop;

import ShopDAOS.DAO.comanda.*;
import ShopDAOS.DAO.empleat.*;
import ShopDAOS.DAO.producte.*;
import ShopDAOS.DAO.proveidor.*;
import ShopDAOS.DAOFactory.DAOFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws SQLException, IOException {
        // Factories
        DAOFactory SQL = DAOFactory.getDAOFactory(DAOFactory.POSTGRESQL);
        DAOFactory MONGO = DAOFactory.getDAOFactory(DAOFactory.MONGODB);

        // Ex 1
        EmpleatDAO empDAO = new EmpleatImpSQL();
        List<Empleat> empleats = empDAO.consultarLlista();
        empDAO = new EmpleatImpFile();
        empDAO.insertarLlista(empleats);

        // Ex 2
        empDAO = new EmpleatImpFile();
        List<Empleat> mongoEmpleats = empDAO.consultarLlista();
        empDAO = new EmpleatImpMongo();
        empDAO.insertarLlista(mongoEmpleats);

        // Ex 3
        ComandaDAO comDAO = new ComandaImpSQL();
        List<Comanda> comandes = comDAO.consultarLlista();
        comDAO = new ComandaImpFile();
        comDAO.insertarLlista(comandes);

        // Ex 4
        comDAO = new ComandaImpFile();
        List<Comanda> mongoComandes = comDAO.consultarLlista();
        comDAO = new ComandaImpMongo();
        comDAO.insertarLlista(mongoComandes);

        // Ex 5
        ProducteDAO prodDAO = new ProducteImpSQL();
        List<Producte> productes = prodDAO.consultarLlista();
        prodDAO = new ProducteImpFile();
        prodDAO.insertarLlista(productes);

        // Ex 6
        prodDAO = new ProducteImpFile();
        List<Producte> mongoProds = prodDAO.consultarLlista();
        prodDAO = new ProducteImpMongo();
        prodDAO.insertarLlista(mongoProds);

        // Ex 7
        ProveidorDAO provDAO = new ProveidorImpSQL();
        List<Proveidor> proveidors = provDAO.consultarLlista();
        provDAO = new ProveidorImpFile();
        provDAO.insertarLlista(proveidors);

        // Ex 8
        provDAO = new ProveidorImpFile();
        List<Proveidor> mongoProvs = provDAO.consultarLlista();
        provDAO = new ProveidorImpMongo();
        provDAO.insertarLlista(mongoProvs);

        // Ex 9
        empDAO = new EmpleatImpMongo();
        Empleat e = empDAO.consultar(7654);
        System.out.println(e);

    }
}
